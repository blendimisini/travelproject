// Login
function loginPopUp(){
    document.getElementById('modal-wrapper').style.display='block';
}

function closePopUp(){
    document.getElementById('modal-wrapper').style.display='none'
}

var modal = document.getElementById('modal-wrapper');
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
//Funksioni per show more tek ofertat permes ajax

$(document).ready(function(){
    var hotelCount = 2;
    $("#showmore").click(function(){
        hotelCount = hotelCount + 2;
        $("#offers").load("loadhotel.php", {
            
            hotelCountnew: hotelCount
            
        });
    });
    
});



// Book
function validateBook() {

    var from = document.forms['book']['checkin'].value;
    var to = document.forms['book']['checkout'].value;
    var country = document.forms['book']['country'].value;
    var checkin = document.getElementById('checkin');
    var checkout = document.getElementById('checkout');
    var countryDOM = document.getElementById('country');

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); 
    var yyyy = today.getFullYear();

    var yearFrom = from.slice(0, 4);
    var mounthFrom = from.slice(5, 7);
    var dayFrom = from.slice(8, 10);

    var yearTo = to.slice(0, 4);
    var mounthTo = to.slice(5, 7);
    var dayTo = to.slice(8, 10);

    checkout.classList.remove('input-error');
    checkin.classList.remove('input-error');
    countryDOM.classList.remove('input-error');

    if (country == '') {
        alert("Select Country");
        countryDOM.classList.add('input-error');
        return false;
    }
    else if (from == '') {
        alert("Empty Date Checkin.");
        checkin.classList.add('input-error');
        return false;
    }
    else if ((yearFrom < yyyy) || (yearFrom >= yyyy && mounthFrom < mm) || (yearFrom >= yyyy && mounthFrom >= mm && dayFrom < dd)) {
        alert("Checkin not valid. Date smaller than current date.");
        checkin.classList.add('input-error');
        return false;
    }
    else if (to == '') {
        alert("Empty Date Checkout.");
        checkout.classList.add('input-error');
        return false;
    }
    else if ((yearTo < yearFrom) || (yearTo >= yearFrom && mounthTo < mounthFrom) || (yearTo >= yearFrom && mounthTo >= mounthFrom && dayTo <= dayFrom)) {
        alert("Checkout not valid. Date smaller than Checkin.");
        checkout.classList.add('input-error');
        return false;
    }


}