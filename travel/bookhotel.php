<?php include 'header.php'; ?>
<?php 
    
    require_once './includes/dbconn.php';

    if (isset($_GET['submit'])) {

        $_SESSION['checkin'] = $_GET['checkin'];
        $_SESSION['checkout'] = $_GET['checkout'];
        $_SESSION['rooms'] = $_GET['rooms'];
    }

    if (isset($_GET['country'])) {
        
        $country = $_GET['country'];
        $room = $_SESSION['rooms'];
    
        $query = $pdo->prepare("SELECT * FROM `hotels` WHERE (`country_id` = :country_id AND `rooms` >= :room)");
        $query->execute(['country_id' => $country, 'room' => $room]);
        $hotels = $query->fetchAll(PDO::FETCH_ASSOC);
        
    }
    else {
        header("Location: ./index.php");
    }

?>
<div id="book-hotel">
    <div class="container">
        <?php foreach ($hotels as $hotel): ?>
        <?php

            $idHot = $hotel['country_id'];
            $query = $pdo->prepare("SELECT * FROM `countries` WHERE `id` = :idhot");   
            $query->execute(['idhot' => $idHot]);
            $nameCountry = $query->fetch();
            
            $idCity = $hotel['city_id'];
            $query = $pdo->prepare("SELECT * FROM `cities` WHERE `id` = :idcity");   
            $query->execute(['idcity' => $idCity]);
            $nameCity = $query->fetch();

            $price = $hotel['current_price'];

            if ($hotel['offered_price'] > 0) {
                $price = $hotel['offered_price'];
            }
        ?>
        <div class="hotel-box">
            <img src="images/<?php echo $hotel['image'];?>" alt="hotlel">
            <h1><?php echo $hotel['name'];?></h1>
            <p><?php echo $nameCountry['name'];?> - <?php echo $nameCity['name'];?> </p>
            <p>Price: <?php echo $price;?> $</p>
            <p>Rooms Available: <?php echo $hotel['rooms'] ?></p>
            <p>Descripton: <?php echo $hotel['description'];?></p>
            <?php 
            if (isset($_SESSION['name'])) {
               $hotelId = $hotel['id'];
               echo "<a href='./includes/addbooked.php?id=$hotelId' class='btn'>BOOK NOW</a>";
            }else {
                echo "<a href='signup.php' class='btn'>Sign Up or LogIn to BOOK</a>";
            }
            ?>
        </div>

        <?php endforeach; ?>
        
    </div>
</div>

<?php include 'footer.php'; ?>