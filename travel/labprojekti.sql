-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2019 at 11:14 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `labprojekti`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `room` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `hotel_id`, `user_id`, `from`, `to`, `room`, `created_at`, `updated_at`) VALUES
(12, 9, 1, '2019-07-10 00:00:00', '2019-07-17 00:00:00', 2, '2019-07-09 00:15:21', '0000-00-00 00:00:00'),
(13, 1, 1, '2019-07-12 00:00:00', '2019-07-19 00:00:00', 1, '2019-07-12 13:13:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `country_id` int(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'Miami Beach', 1, '2019-06-29 00:17:32', '0000-00-00 00:00:00'),
(2, 'Hollywood', 1, '2019-06-29 00:18:50', '0000-00-00 00:00:00'),
(3, 'Wailea', 1, '2019-06-29 00:19:09', '0000-00-00 00:00:00'),
(4, 'Durres', 2, '2019-06-29 00:19:25', '0000-00-00 00:00:00'),
(5, 'Golem', 2, '2019-06-29 00:19:37', '0000-00-00 00:00:00'),
(6, 'Sarande', 2, '2019-06-29 00:19:46', '0000-00-00 00:00:00'),
(7, 'Vlore', 2, '2019-06-29 00:20:19', '0000-00-00 00:00:00'),
(8, 'Ksamil', 2, '2019-06-29 00:20:31', '0000-00-00 00:00:00'),
(9, 'Antalay', 3, '2019-06-29 00:20:43', '0000-00-00 00:00:00'),
(10, 'Alanya', 3, '2019-06-29 00:21:26', '0000-00-00 00:00:00'),
(11, 'Istambul', 3, '2019-06-29 00:21:46', '0000-00-00 00:00:00'),
(12, 'Crete Region', 4, '2019-06-29 00:22:20', '0000-00-00 00:00:00'),
(13, 'Zakynthos Island', 4, '2019-06-29 00:22:41', '0000-00-00 00:00:00'),
(14, 'Santorini', 4, '2019-06-29 00:23:02', '0000-00-00 00:00:00'),
(15, 'Dubrovnik', 5, '2019-06-29 00:24:41', '0000-00-00 00:00:00'),
(16, 'Split', 5, '2019-06-29 00:24:54', '0000-00-00 00:00:00'),
(17, 'Sardinia', 6, '2019-06-29 00:25:16', '0000-00-00 00:00:00'),
(18, 'Amalfi', 6, '2019-06-29 00:25:27', '0000-00-00 00:00:00'),
(19, 'Ibiza', 7, '2019-06-29 00:25:44', '0000-00-00 00:00:00'),
(21, 'Mallorca', 7, '2019-06-29 00:26:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_name`, `user_id`, `text`, `hotel_id`, `created_at`) VALUES
(2, 'Blendi', 1, 'dafdsafd', 13, '2019-07-11 13:23:27'),
(4, 'Blendi', 1, 'asdfsa', 13, '2019-07-11 13:24:43'),
(5, 'Blendi', 1, 'asdfsa', 13, '2019-07-11 13:25:24'),
(46, 'Blendi', 1, 'asdfsagfds', 10, '2019-07-11 22:02:15'),
(48, 'Blendi', 1, 'qendrim', 9, '2019-07-11 22:13:08'),
(49, 'Blendi', 1, '2134resrgsdfqendrim', 9, '2019-07-11 22:13:13');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'United State', '2019-06-29 00:09:30', '0000-00-00 00:00:00'),
(2, 'Albania', '2019-06-29 00:11:24', '0000-00-00 00:00:00'),
(3, 'Turkey', '2019-06-29 00:11:24', '0000-00-00 00:00:00'),
(4, 'Greece', '2019-06-29 00:12:01', '0000-00-00 00:00:00'),
(5, 'Croatia', '2019-06-29 00:12:20', '0000-00-00 00:00:00'),
(6, 'Italy', '2019-06-29 00:12:32', '0000-00-00 00:00:00'),
(7, 'Spain', '2019-06-29 00:12:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `current_price` int(11) NOT NULL,
  `offered_price` int(11) NOT NULL,
  `rooms` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `name`, `description`, `image`, `current_price`, `offered_price`, `rooms`, `company_name`, `country_id`, `city_id`, `created_at`, `updated_at`) VALUES
(0, 'BlendiHoteltest', 'Kompani per pushimet tuaja verore', 'c4491ef96d6eddc2c6c21b06c87ce9b9.jpg', 251, 123, 28, 'Blendi', 1, 1, '0000-00-00 00:00:00', '2019-07-01 15:48:14'),
(1, 'BlendiHotel', 'Kompani per pushimet tuaja verore', 'bmw.jpg', 251, 123, 18, 'Blendi', 1, 1, '2019-06-30 20:05:37', '0000-00-00 00:00:00'),
(2, 'BlendiHoteltest', 'Blendi hotel ne USA ofron akomodim perfekt', 'contactus.jpg', 321, 200, 19, 'Blendi', 1, 2, '0000-00-00 00:00:00', '2019-07-02 23:42:44'),
(4, 'QendrimHotel', 'Nje vend ndryshe', 'bmw.jpg', 256, 22, 24, 'Blendi', 1, 1, '0000-00-00 00:00:00', '2019-07-05 16:22:39'),
(5, 'BlendiHotel', 'Kompani per pushimet tuaja verore test test', 'attack-on-titan-2-cover-uhd-4k-wallpaper.jpg', 255, 200, 0, 'Blendi', 1, 1, '2019-07-01 15:57:05', '2019-07-02 23:48:47'),
(6, 'Blendi Misini ', 'Kompani per pushimet tuaja verore test test test test', 'offers.jpg', 222, 200, 0, 'Blendi', 2, 8, '2019-07-01 15:59:42', '2019-07-02 17:28:56'),
(7, 'Blendi Misini testi fundit', 'Kompani per pushimet tuaja verore', 'benzi1.jpg', 999, 888, 10, 'Blendi', 1, 1, '2019-07-01 16:00:14', '2019-07-05 16:16:15'),
(8, 'Hoteli qendrimi', 'Vend ndryshe', 'audi.jpg', 888, 22, -1, 'Qendrim Vrella', 3, 10, '0000-00-00 00:00:00', '2019-07-01 16:14:27'),
(9, 'qednrimi 2 xxxxxxxx', 'xxxxxxxxxxxxxxxxx', 'universe.jpg', 6666, 444, 0, 'Qendrim Vrella', 2, 5, '2019-07-01 16:15:25', '2019-07-01 16:16:32'),
(10, 'FitimHotel', 'Kompani per pushimet tuaja verore', 'lambo.jpg', 999, 20, 51, 'Blendi', 1, 1, '2019-07-05 14:44:11', '0000-00-00 00:00:00'),
(11, 'MuhamedHotel', 'Blendi hotel ne USA ofron akomodim perfekt', 'i-m-hotel-lobby.jpg', 222, 0, 10, 'Blendi', 1, 1, '2019-07-10 00:01:43', '0000-00-00 00:00:00'),
(12, 'MuhamedHotel', 'Blendi hotel ne USA ofron akomodim perfekt', 'i-m-hotel-lobby.jpg', 222, 0, 10, 'Blendi', 1, 1, '2019-07-10 00:02:08', '0000-00-00 00:00:00'),
(13, 'muhametxxxx', 'Blendi hotel ne USA ofron akomodim perfekt', 'i-m-hotel-lobby.jpg', 5, 22, 99999, 'Blendi', 4, 13, '2019-07-10 00:02:41', '2019-07-10 00:26:10');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `current_price` int(11) NOT NULL,
  `offer_price` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `name`, `description`, `image`, `current_price`, `offer_price`, `country_id`, `company_name`, `created_at`, `updated_at`) VALUES
(1, 'Blendi Misini', 'Kompani per pushimet tuaja verore', '', 0, 0, 0, '', '2019-06-30 15:50:15', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(512) NOT NULL,
  `is_company` tinyint(4) NOT NULL,
  `is_admin` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_company`, `is_admin`, `created_at`, `updated_at`) VALUES
(1, 'Blendi', 'blendimisini@gmail.com', '$2y$10$vNvknl04zoAhDb9RNhPtFOe.v35StgvfJJ9/ECi3Ix0b1rzNbCiqC', 1, 0, '2019-06-28 08:50:15', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `city_id` (`city_id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  ADD CONSTRAINT `books_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);

--
-- Constraints for table `hotels`
--
ALTER TABLE `hotels`
  ADD CONSTRAINT `hotels_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `hotels_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
