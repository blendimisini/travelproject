<?php 
include "includes/login.inc.php";
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Travel</title>
    <link rel="stylesheet" href="./css/style.css">
    <script src="./js/jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="./js/getState.js"></script>
</head>
<body>
    <!-- Login -->
    <div id="modal-wrapper">
        <div class="modal-content">
            <div class="modal-logclose">
                <h1>Log In</h1>
                <p onclick="closePopUp()" class="close">&times;</p>
            </div>    
            
            <form action="<?php $_SERVER['PHP_SELF'];?>" method="POST">
                <input type="email" placeholder="Email" name="email" value="<?php echo $email; ?>" autofocus>
                <span class="error-font"><?php echo $email_error; ?></span>
                <input type="password" placeholder="Password" name="password">  
                <span class="error-font"><?php echo $password_error; ?></span>    
                <button type="submit" name="login-submit" class="btn">LogIn</button>
            </form>
            <p>Create an account? <a href="signup.php">Sign Up</a></p>
        </div>
    </div>

    <header>
        <div class="container">
            <h1>Travel</h1>
            <?php if(isset($_SESSION['isadmin']) && $_SESSION['isadmin'] == "1"): ?>
                    <button id="toggle"></button>
            <?php endif; ?>
            
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About us</a></li>
                <li><a href="hotel.php">Hotels</a></li>
                <li><a href="offers.php">Offers</a></li>
                <li><a href="contact.php">Contact</a></li>
                <?php if(isset($_SESSION['iscompany']) && $_SESSION['iscompany'] == "1"): ?>
                    <li><a href="AddHotels.php">Add Hotels</a></li>
                <?php endif; ?>
            </ul>
            <ul>
                <?php
                    if(!isset($_SESSION['name'])){
                        echo '<li><a class="btn" href="signup.php">Sign Up</a></li>';
                        echo '<li><a class="btn" href="javascript:loginPopUp()">Login</a></li>';
                    }elseif(isset($_SESSION['name'])){
                        echo '<li><a class="btn" href="includes/logout.inc.php">Loguot</a></li>';
                        if($_SESSION['iscompany'] != '1' && $_SESSION['isadmin'] != '1'){
                            echo '<li><a class="btn" href="book.php">Book Now</a></li>';
                            echo '<li><a class="btn" href="mybooks.php">My Books</a></li>';
                        }
                    }          
                ?>
            </ul>
        </div>        
        
    </header>
    <ul id="togglenav">
            <li><a href="deleteusers.php">Delete user</a></li>
            <li><a href="adminadd.php">Add admin privileges</a></li>
        
            
        </ul>
    <script src="js/toggle.js"></script>
    