<?php
 include 'header.php';
 ?>
    <div id="hotels">
        <div class="container">
            <?php
                include "includes/dbconn.php";
                $select = $pdo->prepare("SELECT * FROM hotels ORDER BY id DESC");
                $select->execute();
                $hotels = $select->fetchAll(PDO::FETCH_ASSOC);
                ?>

                <?php foreach($hotels as $hotel): ?>
                    <div class="hotel">
                    <p>Comments</p>

                        <h2><?php echo $hotel['name'] ?></h2>
                        <img src=images/<?php echo $hotel['image'] ?> width="250px" alt="does not exist!">
                        <span><?php echo $hotel['description'] ?></span>
                        <?php if($hotel['offered_price'] != '0'){

                            echo "<strong><span>".$hotel["offered_price"]."EURO-OFFERT</span></strong>";

                        }else{
                           echo  "<strong><span>". $hotel["current_price"]."  EURO</span></strong>";
                        }
                          
                        ?>
                        <?php if(isset($_SESSION['iscompany']) && $_SESSION['iscompany'] == '1' ) : ?>
                        <?php if(isset($_SESSION['name']) && $_SESSION['name'] == $hotel['company_name'] ) : ?>
                        <a href="makeoffer.php?id=<?php echo $hotel['id'] ?>">Make offer</a>
                        <?php endif;?>
                        <?php endif;?>
                    </div>
                <?php endforeach; ?>
        </div>
    </div>



<?php include 'footer.php'; ?>