<?php include 'header.php'; ?>
<?php include 'includes/comment.php'?>
<?php
    require 'includes/dbconn.php';
    $hotels = $pdo->prepare("SELECT  * FROM hotels WHERE id = :id");
    $hotels->bindParam(':id',$_GET['id']);
    $hotels->execute();
	$query ="SELECT * FROM countries";
	$results = $pdo->query($query);
?> 

<div id="book-con">
    <h1>Offers</h1>
    <form class="book-form" action="bookhotel.php" method="GET" name="book" onsubmit="return validateBook()">
        <div class="input-con">
            <label for="country">Country</label>
            <select  name="country" id="country">
					<option value disabled selected >Select Country</option>
					<?php foreach($results as $country): ?> 
						<option value="<?php echo $country["id"];?>"><?php echo $country["name"]; ?></option>
					<?php endforeach;?>
			</select>
        </div>
        <div class="input-con">
            <label for="checkin">Check In</label>
            <input type="date" name="checkin" id="checkin">
        </div>    
        <div class="input-con">
            <label for="checkout">Check Out</label>
            <input type="date" name="checkout" id="checkout">
        </div> 
        <div class="input-con">
            <label for="checkout">Rooms</label>
            <select name="rooms" id="rooms">
					<option value="1">1</option>
					<option value="2">2</option>
			</select>
        </div> 
        <div class="input-con">   
            <button type="submit" name="submit" value="submit" class="btn">Search</button>
        </div> 
    </form>
    <p>Search in witch country you want to travel</p>
</div>
<div class="container">
    <?php foreach($hotels as $hotel): ?>
    <?php if($hotel['offered_price'] > '0'): ?>
    <?php

$idHot = $hotel['country_id'];
$query = $pdo->prepare("SELECT name FROM `countries` WHERE `id` = :idhot");   
$query->execute(['idhot' => $idHot]);
$nameCountry = $query->fetch();
$nameCountry = array_unique($nameCountry);
$string_versionCountry = implode('.',$nameCountry);

$idCity = $hotel['city_id'];
$query = $pdo->prepare("SELECT name FROM `cities` WHERE `id` = :idcity");   
$query->execute(['idcity' => $idCity]);
$nameCityArr = $query->fetch();
$nameCityArr = array_unique($nameCityArr);
$string_versionCity = implode('.', $nameCityArr)
?>
    <div id="offers">
        <div class="img-offers">
            <img src="images/<?php echo $hotel['image']; ?>" alt="offer image">
        </div>
        <div class="offers-content">
            <div class="header-offers">
                <p class="offer-price">Only <?php echo $hotel['offered_price'];?>€</p>
                <p class="offer-date"><?php echo $hotel['updated_at']; ?></p>
            </div>
            <div class="titulli-offertes">
                <h3><?php echo $hotel['name']; ?></h3>
            </div>
            <div class="info-offertes">
                <p><a href="#"><?php echo $string_versionCity; ?>-<?php echo $string_versionCountry; ?></a></p>
                <p><a href="#"></a><?php echo $hotel['company_name']; ?></p>
                <p><del><?php echo $hotel['current_price']; ?>Euro</del></p>
                <p><a href="./comments.php?id=<?php echo $hotel['id']; ?>">Comments</a></p>
            </div>
            <div class="teksti-offertes">
                <p><?php echo $hotel['description']; ?></p>
            </div>
            <?php if(isset($_SESSION['name'])) :?>
            <div class="button-book">
                <a href="./bookoffers.php?id=<?php echo $hotel['id']; ?>" class="button-book-btn">BOOK NOW</a>
            </div>
            <?php endif;?>
            <?php if(!isset($_SESSION['name'])) :?>
            <div class="offer-login">
            <p class="offer-login">Login or</p>
            <a href="signup.php">Signup to Book it</a>
            </div>
            <?php endif;?><br>
        </div>
    </div>
<?php endif;?>
<?php endforeach;?>
<h1>Comments</h1><br>
<?php 
    $comments = $pdo->prepare('SELECT * FROM comments where hotel_id = :id ORDER BY id DESC ');
    $comments->bindParam(':id',$_GET['id']);
    $comments->execute();
   
?>
<?php foreach($comments as $comment):?>
<div id="comments-section">
    <div class="comments-details">
    <h3><?php echo $comment['user_name']?></h3>
    <p><?php echo $comment['created_at']?></p>
   </div>
   <div class="comments-description">
       <p><?php echo $comment['text']?></p>
   </div>
   <?php if($_SESSION['id'] == $comment['user_id']):?>
    <a href="includes/deletecomments.php?id=<?php echo $comment['id']?>">Delete</a>
<?php endif;?>
</div><br>
<?php endforeach;?>

   <form action="<?php $_SERVER['PHP_SELF'];?>" method="POST" id="comment-from">
       <label for="text-comments">Enter your comment:</label><br>
       <input type="text" name="text" class="text-comments" id="<?php echo $field_error?>" >
       <span class="error-font"><?php echo $text_error ?></span>
       <br><br>
       <span class="success-font"><?php echo $success ?></span>
       <br>
       <input type="submit" value="Submit" class="btn">
   </form>
</div> 
<?php include 'footer.php'; ?>