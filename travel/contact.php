<?php 
    include 'header.php';
    include 'includes/contact.inc.php';
?>

<div class="form-contactus">
  <div class="contact-form">
    <div class="contact-input">
    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
      <input type="text" class="input-contact" placeholder="name" name="name" value="<?php echo $name;?>">
        <span class="error-font"><?php echo $name_error;?></span>
      <input type="text" class="input-contact" placeholder="Email Address" name="email" value="<?php echo $email;?>">
        <span class="error-font"><?php echo $email_error;?></span>
      <input type="text" class="input-contact" placeholder="Subject" name="subject" value="<?php echo $subject;?>">
        <span class="error-font"><?php echo $subject_error;?></span>
    </div>
    <div class="msg-form">
        <textarea placeholder="Message" name="message"><?php echo $message;?></textarea>
        <span class="error-font"><?php echo $message_error;?></span>
        <button class="btn-form">Send</button>
        <span class="success-font"><?php echo $succes;?></span>
    </div>
  </div>
    </form>
    <div class="ifram-contact">  
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d736.9450523719692!2d21.154659977648656!3d42.36852313862664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x904efc11559725f4!2sUniversiteti+%22UBT%22+Ferizaj!5e0!3m2!1sen!2s!4v1558531431493!5m2!1sen!2s" width="600"   height="250" frameborder="0" style="border:0" allowfullscreen>
        </iframe>
    </div>
</div>	

<?php include 'footer.php';?>