<?php
    $email_error = $password_error = "";
    $email = $password = "";
    $success="";

    function test_input($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


    if($_SERVER["REQUEST_METHOD"] == "POST"){
        require_once 'dbconn.php';
       
        if(empty($_POST["email"])){
                $email_error = "Email is required";
        }else{
            
            $email = test_input($_POST["email"]);
            
        }

        if(empty($_POST["password"])){
            $password_error = "Password is required!";
        }else{
           $password = test_input($_POST['password']);
        }

        $select = $pdo->prepare("SELECT * FROM users WHERE  Email = :email");
        $select->execute([":email"=>$email]);
        $pw = $select->fetch(PDO::FETCH_ASSOC);
        
        if($pw != false){           
            $val = password_verify($password,$pw['password']);
            
            if($val == true){
                session_start();
                $_SESSION['id'] = $pw['id'];
                $_SESSION['name']=$pw['name'];
                $_SESSION['email']=$pw['email'];
                $_SESSION['iscompany']=$pw['is_company'];
                $_SESSION['isadmin']=$pw['is_admin'];
                $_SESSION['created_at']=$pw['created_at'];
                $_SESSION['updated_at']=$pw['updated_at'];
                $_SESSION['password']=$pw['password'];
                header("Location: index.php?login=success");
            }else{
                $password_error = "Incorrect password!";
            }
        }else{
            $userid_error = "Username or email does not exists!";
        }

    }