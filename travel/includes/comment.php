
<?php 

$text_error = $field_error = "";
$text = $success =  "";
$user_name = $_SESSION['name'];
$user_id = $_SESSION['id'];
$hotel_id = $_GET['id'];

if($_SERVER["REQUEST_METHOD"] == "POST"){

    require 'dbconn.php';

    if(empty($_POST['text'])){
        $text_error = 'Enter a comment!';
        $field_error = 'error-border';
        
    }
    if($text_error == ""){
        $text = $_POST['text'];

        $insert = $pdo->prepare('INSERT INTO `comments`(`user_name`, `user_id`, `text`, `hotel_id`) VALUES (:user_name,:user_id,:text,:hotel_id)');
        $insert->bindParam(':user_name',$user_name);
        $insert->bindParam(':user_id',$user_id);
        $insert->bindParam(':text',$text);
        $insert->bindParam(':hotel_id',$hotel_id);
        $insert->execute();

        $success="Komentuat!";
        
        $text_error = $field_error =""; 


    }
}
