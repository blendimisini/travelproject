<?php 
    require 'dbconn.php';
    $query = $pdo->prepare('DELETE FROM comments WHERE id = :id');
    $query->bindParam(':id', $_GET['id']);
    $query->execute();
    header('Location: ' . $_SERVER['HTTP_REFERER']);