<?php 

    include 'header.php';
    
    if(isset($_SESSION['iscompany'])){
        if($_SESSION['iscompany'] == '1'){
            include "includes/hoteladd.php";
        }else{
            die('Only companies are allowed to add hotels!');
        }
    }else{
        die('You are not loged in!');      
    }


    require_once 'includes/hoteladd.php';

	require_once("includes/dbconn.php");
	$query ="SELECT * FROM countries";
	$results = $pdo->query($query);
?>
<div id="signup-con">
	<form action="<?php $_SERVER['PHP_SELF'];?>" method="POST" enctype="multipart/form-data">
			<div class="input-con">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="<?php echo $name?>">
                <span class="error-font"><?php echo $name_error; ?></span>
            </div>
			<div class="input-con">
                <label for="description">Description</label>
                <input type="text" name="description" id="description" value="<?php echo $description?>">
                <span class="error-font"><?php echo $description_error?></span>
            </div>
			<div class="input-con">
                <label for="image">Image</label>
                <input type="file" name="image" >
                <span class="error-font"><?php echo $file_error?></span>
            </div>
			<div class="input-con">
                <label for="current_price">Price</label>
                <input type="text" name="current_price" id="current_price" value="<?php echo $current_price?>">
                <span class="error-font"><?php echo $current_price_error?></span>
            </div>
			<div class="input-con">
                <label for="rooms">Rooms</label>
                <input type="text" name="rooms" id="rooms" value="<?php echo $rooms?>">
                <span class="error-font"><?php echo $rooms_error?></span>
            </div>
			<div class="input-con">
				<label for="contry-list">Country:</label><br/>
				<select  name="country" id="country-list" onChange="getState(this.value);">
					<option value disabled selected >Select Country</option>
					<?php foreach($results as $country): ?> 
						<option value="<?php echo $country["id"];?>"><?php echo $country["name"]; ?></option>
					<?php endforeach;?>
				</select><br>
				<span class="error-font"><?php echo $contry_error?></span>
			</div>
			<div class="input-con">
				<label for="state-list">City</label><br/>
				<select name="city" id="state-list" onChange="getCity(this.value);">
					<option value="">Select State</option>
				</select>
				<span class="error-font"><?php echo $city_error?></span>
				<span class="success-font"><?php echo $success?></span>
			</div>
		<input type="submit" name="submit" class="btn">
	</form>
</div>
<?php include 'footer.php'; ?>