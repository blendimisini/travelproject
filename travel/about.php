<?php include 'header.php'; ?>
<?php
	require_once("includes/dbconn.php");
	$query ="SELECT * FROM countries";
	$results = $pdo->query($query);
?>
    <div id="banner">
        <div class="container">
            <h1>About us</h1>
            <form class="book-form" action="" name="book" onsubmit="return validateBook()">
                    <div class="input-con">
                        <label for="country">Country</label>
                        <select  name="country" id="country">
                                <option value disabled selected >Select Country</option>
                                <?php foreach($results as $country): ?> 
                                    <option value="<?php echo $country["id"];?>"><?php echo $country["name"]; ?></option>
                                <?php endforeach;?>
                        </select>
                    </div>
                    <div class="input-con">
                        <label for="checkin">Check In</label>
                        <input type="date" name="checkin" id="checkin">
                    </div>    
                    <div class="input-con">
                        <label for="checkout">Check Out</label>
                        <input type="date" name="checkout" id="checkout">
                    </div> 
                    <div class="input-con">
                        <label for="checkout">Rooms</label>
                        <select name="rooms" id="rooms">
                                <option value="1">1</option>
                                <option value="2">2</option>
                        </select>
                    </div> 
                    <div class="input-con">   
                        <button type="submit" class="btn">Search</button>
                    </div> 
            </form>
        </div>
    </div>
    <div id="content">
        <div class="container">
            <div class="content-img">
                <img src="img/hotel.jpeg" alt="image does not exist" width="100%" height="400px">
            </div>
            <div class="content-txt">
                <h1>Beach Hotel - More than a stay</h1>
                <p>In vitae nisi aliquam, scelerisque leo a, volutpat sem. Vivamus rutrum dui fermentum eros hendrerit, id lobortis leo volutpat. Maecenas sollicitudin est in libero pretium interdum.</p>
                <p>In vitae nisi aliquam, scelerisque leo a, volutpat sem. Vivamus rutrum dui fermentum eros hendrerit, id lobortis leo volutpat. Maecenas sollicitudin est in libero pretium interdum. Nullam volutpat dui sem, ac congue purus hendrerit, id lobortis leo luctus nec. In vitae nisi aliquam, scelerisque. Vivamus rutrum dui fermentum eros hendrerit, id lobortis leo volutpat. Maecenas sollicitudin est in libero pretium interdum. Nullam volutpat dui sem, ac congue purus hendrerit, id lobortis leo luctus nec.</p>
            </div>
            <div class="content-txt">
                <h1>Amazing Facilities</h1>
                <p>In vitae nisi aliquam, scelerisque leo a, volutpat sem. Vivamus rutrum dui fermentum eros hendrerit, id lobortis leo volutpat. Maecenas sollicitudin est in libero pretium interdum.</p>
                <div>
                    <img width="40px" src="img/hotel_icon.png" alt="image does not exist">
                    <h2>Beautiful Rooms</h2>
                    <p>In vitae nisi aliquam, scelerisque leo a, volutpat sem. Vivamus rutrum.</p>
                </div>
                <div>
                    <img width="40px" src="img/swimming_icon.png" alt="image does not exist">
                    <h2>Swimming Pools</h2>
                    <p>In vitae nisi aliquam, scelerisque leo a, volutpat sem. Vivamus rutrum.</p>
                </div>
                <div>
                    <img width="40px" src="img/beach_icon.png" alt="image does not exist">
                    <h2>Luxury Resort</h2>
                    <p>In vitae nisi aliquam, scelerisque leo a, volutpat sem. Vivamus rutrum.</p>
                </div>
            </div>
            <div class="content-img">
                <img src="img/hotel2.jpg" alt="image does not exist" width="100%" height="400p">
            </div>
        </div>
    </div>
    <div id="page-info">
        <div class="container">
            
            <div class="page-info-content">
                <img src="img/icon_4.svg" alt="image does not exist" width="100px">
                <div>
                    <span>25k</span>
                    <p>Clients since opening</p>
                </div>
            </div>
            
            <div class="page-info-content">
                <img src="img/icon_1.svg" alt="image does not exist" width="100px">
                <div>
                    <span>120</span>
                    <p>Hotel rooms</p>
                </div>
            </div>
            
            <div class="page-info-content">
                <img src="img/icon_5.svg" alt="image does not exist" width="100px">
                <div>
                    <span>7</span>
                    <p>One day cruises</p>                
                </div>
            </div>
            
        </div>
    </div>

<?php include 'footer.php'; ?>