<?php
    include 'header.php'; 
    if(isset($_SESSION['iscompany']) && isset($_SESSION['isadmin'])){
        if($_SESSION['iscompany'] == '1' || $_SESSION['isadmin'] == '1'){
            die('Companies and admins are not allowed to book hotels!!');
        }
    }else{
        die('You are not loged in!');      
    }

?>
<?php
	require_once("includes/dbconn.php");
	$query ="SELECT * FROM countries";
	$results = $pdo->query($query);
?>


<div id="book-con">
    <h1>Search For Hotels</h1>
    <form class="book-form" action="bookhotel.php" method="GET" name="book" onsubmit="return validateBook()">
        <div class="input-con">
            <label for="country">Country</label>
            <select  name="country" id="country">
					<option value disabled selected >Select Country</option>
					<?php foreach($results as $country): ?> 
						<option value="<?php echo $country["id"];?>"><?php echo $country["name"]; ?></option>
					<?php endforeach;?>
			</select>
        </div>
        <div class="input-con">
            <label for="checkin">Check In</label>
            <input type="date" name="checkin" id="checkin">
        </div>    
        <div class="input-con">
            <label for="checkout">Check Out</label>
            <input type="date" name="checkout" id="checkout">
        </div> 
        <div class="input-con">
            <label for="checkout">Rooms</label>
            <select name="rooms" id="rooms">
					<option value="1">1</option>
					<option value="2">2</option>
			</select>
        </div> 
        <div class="input-con">   
            <button type="submit" name="submit" value="submit" class="btn">Search</button>
        </div> 
    </form>
    <p>Search in witch country you want to travel</p>
</div>

<?php include 'footer.php'; ?>