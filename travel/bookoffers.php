<?php include 'header.php'; ?>
<?php include 'includes/bookoffers.php'?>
<?php

    $_SESSION['hotel_id'] = $_GET['id'];
?>


<div id="book-con">
    <h1>Select date and rooms</h1>
    <form class="book-form" action="<?php $_SERVER['PHP_SELF'];?>" method="POST">
        
        <div class="input-con">
            <label for="checkin">Check In</label>
            <input type="date" name="checkin" id="checkin" value="<?php echo $checkin ?>">
            <span class="error-font" style="color:red;"><?php echo $checkin_error; ?></span>

        </div>    
        <div class="input-con">
            <label for="checkout">Check Out</label>
            <input type="date" name="checkout" id="checkout" value="<?php echo $checkout ?>">
            <span class="error-font"><?php echo $checkin_error; ?></span>
        </div> 
        <div class="input-con">
            <label for="checkout">Rooms</label>
            <select name="rooms" id="rooms">
					<option value="1">1</option>
					<option value="2">2</option>
			</select>
        </div> 
        <div class="input-con">   
            <button type="submit" name="submit" value="submit" class="btn">Book now</button>
        </div> 
    </form>
    <p>Search in witch country you want to travel</p>
</div>

<?php include 'footer.php'; ?>