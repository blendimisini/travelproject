<?php

session_start();
require_once './includes/dbconn.php';
require_once __DIR__ . '/vendor/autoload.php';

$userId = $_SESSION['id'];
$output = '';

$query = $pdo->prepare("SELECT * FROM `users` WHERE `id` = :id");
$query->execute(['id' => $userId]);
$users = $query->fetch(PDO::FETCH_ASSOC);


$query = $pdo->prepare("SELECT * FROM `books` WHERE `user_id` = :id");
$query->execute(['id' => $userId]);
$books = $query->fetchAll(PDO::FETCH_ASSOC);

$output .= '<div class="top"><h1>Travel</h1><h4>Owner: '. $users['name'] .'</h4></div>';

foreach ($books as $book) {

    $hotelId = $book['hotel_id'];

    $query = $pdo->prepare("SELECT * FROM `hotels` WHERE `id` = :id");
    $query->execute(['id' => $hotelId]);
    $hotels = $query->fetch(PDO::FETCH_ASSOC);

    $countryId = $hotels['country_id'];
    $cityId = $hotels['city_id'];

    $query2 = $pdo->prepare("SELECT `name` FROM `countries` WHERE `id` = :id");
    $query2->execute(['id' => $countryId]);
    $countries = $query2->fetch(PDO::FETCH_ASSOC);

    $query3 = $pdo->prepare("SELECT `name` FROM `cities` WHERE `id` = :id");
    $query3->execute(['id' => $cityId]);
    $cities = $query3->fetch(PDO::FETCH_ASSOC);

    $to = substr($book['to'], 8, 2);
    $mounth = substr($book['to'], 5, 2) - substr($book['from'], 5, 2);

    if (substr($book['to'], 5, 2) > substr($book['from'], 5, 2)) {
        for ($i = 0; $i < $mounth; $i++) {
            $to += 30;
        }
        $days = $to - substr($book['from'], 8, 2);
    }else{
        $days = $to - substr($book['from'], 8, 2);
    }

    $price = $hotels['current_price'] * $days * $book['room'];

    if ($hotels['offered_price'] > 0) {
        $price = $hotels['offered_price'] * $days * $book['room'];
    }

    $output .= '<div class="book-cont"><p><strong>Book ID: </strong>' .  $book["id"] . '</p>
                <p><strong>Hotel Name: </strong>' .  $hotels["name"] . '</p>
                <p><strong>Country: </strong>' .  $countries["name"] . '</p>
                <p><strong>City: </strong>' .  $cities["name"] . '</p>
                <p><strong>Check In: </strong>' .  substr($book["from"], 0, 10) . '</p>
                <p><strong>Check Out: </strong>' .  substr($book["to"], 0, 10) . '</p>
                <p><strong>Days: </strong>' .  $days . '</p>
                <p><strong>Rooms: </strong>' .  $book["room"] . '</p>
                <p><strong>Price: </strong>' .  $price . '&euro;</p>
                <p><strong>Booked At: </strong>' .  $book["created_at"] . '</p><hr></div>';
}


$mpdf = new \Mpdf\Mpdf();

$stylesheet = file_get_contents('./css/pdf.css');

$mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($output,\Mpdf\HTMLParserMode::HTML_BODY);
// $mpdf->WriteHTML($output);

$mpdf->Output('mybook.pdf', 'I');

